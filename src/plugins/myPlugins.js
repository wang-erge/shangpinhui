//Vue插件一定得暴露一个对象，且必须有 install 方法
let myPlugins = {}

// Vue就是vue对象，options 是在main.js 使用时传入的对象
myPlugins.install = function (Vue, options) {
  // console.log(options) //获取了使用插件时传入的name
  //全局指令
  Vue.directive(options.name, (element, params) => {
    element.innerHTML = params.value.toUpperCase()
    console.log(params)
  })
}
//对外暴露组件对象
export default myPlugins
