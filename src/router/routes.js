/* 
所有静态路由配置的数组
*/
export default [
  {
    name: 'home',
    path: '/home',
    component: () => import('../views/Home/index.vue'),
    meta: {
      show: true
    }
  },
  {
    path: '/',
    redirect: '/home'
  },

  {
    name: 'search',
    path: '/search/:keyword?', // params参数可传可不传
    component: () => import('../views/Search/index.vue'),
    // 将params参数和query参数映射成属性传入路由组件
    props: (route) => ({
      keyword3: route.params.keyword,
      keyword4: route.query.keyword2
    }),
    meta: {
      show: true
    }
  },
  {
    name: 'detail',
    path: '/detail/:skuid',
    component: () => import('../views/Detail/index.vue')
  },
  {
    name: 'addcartsuccess',
    path: '/addcartsuccess',
    component: () => import('../views/AddCartSuccess/index.vue')
    /* beforeEnter(to, from, next) {
      // 得到当前路由信息对象
      // const route = router.currentRoute  // route就是from
      // 得到要跳转到目路由的query参数
      const skuNum = to.query.skuNum
      // 读取保存的数据
      const skuInfo = JSON.parse(window.sessionStorage.getItem('SKU_INFO_KEY'))
      console.log('---', skuNum, skuInfo)
      // 只有都存在, 才放行
      if (skuNum && skuInfo) {
        next()
      } else {
        // 在组件对象创建前强制跳转到首页
        next('/')
      }
    } */
  },
  {
    name: 'shopcart',
    path: '/shopcart',
    component: () => import('../views/ShopCart/index.vue'),
    meta: {
      show: true
    }
  },
  {
    name: 'trade',
    path: '/trade',
    component: () => import('../views/Trade/index.vue'),
    meta: {
      show: true
    },
    /* 路由独享守卫： 只能从购物车界面跳转到交易界面 */
    beforeEnter(to, from, next) {
      if (from.path === '/shopcart') {
        next()
      } else {
        next('/shopcart')
      }
    }
  },
  {
    name: 'pay',
    path: '/pay',
    component: () => import('../views/Pay/index.vue'),
    meta: {
      show: true
    },
    beforeEnter(to, from, next) {
      if (from.path === '/trade') {
        next()
      } else {
        // next(false)  // 重置跳转到 from 页面(注意： 在history模式中会有问题)
        next('/tradet')
      }
    }
  },
  {
    path: '/paysuccess',
    component: () => import('../views/Pay/PaySuccess.vue'),
    /* 只有从支付界面, 才能跳转到支付成功的界面 */
    beforeEnter(to, from, next) {
      if (from.path === '/pay') {
        next()
      } else {
        next('/pay')
      }
    }
  },
  {
    // name: 'center',
    path: '/center',
    component: () => import('../views/Center/index.vue'),
    meta: {
      show: true
    },
    children: [
      {
        path: 'myorder', //  path值为空字符串,此时Tab1为默认子路由，一进入center页面，会默认展示myOrder
        component: () => import('../views/Center/myOrder.vue')
      },
      {
        path: 'grouporder',
        component: () => import('../views/Center/groupOrder.vue')
      },
      {
        path: '/center',
        redirect: '/center/myorder'
      }
    ]
  },

  {
    path: '/register',
    component: () => import('../views/Register/index.vue'),
    meta: {
      isHideFooter: true
    }
  },

  {
    path: '/login',
    component: () => import('../views/Login/index.vue'),
    meta: {
      isHideFooter: true
    }
  }
]
