import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes.js'
import store from '@/store'

// 声明使用插件
Vue.use(VueRouter)

// 向外默认暴露路由器对象
let router = new VueRouter({
  mode: 'history', // 没有#的模式
  routes, // 注册所有路由
  //router-link跳转时回到顶部
  scrollBehavior(to, from, savedPosition) {
    //返回的这个y=0，代表的滚动条在最上方
    return { y: 0 }
  }
})

//全局守卫：前置守卫（在路由跳转之间进行判断）
router.beforeEach(async (to, from, next) => {
  //用户登录了
  if (store.state.user.token) {
    if (to.path == '/login' || to.path == '/register') {
      next('/')
    } else {
      //已经登陆了,访问的是非登录与注册
      //登录了且拥有用户信息放行
      if (store.state.user.userInfo.name) {
        next()
      } else {
        //登陆了且没有用户信息
        //在路由跳转之前获取用户信息且放行
        try {
          await store.dispatch('getUserInfo')
          next()
        } catch (error) {
          //token失效从新登录
          await store.dispatch('userLogout')
          next('/login')
        }
      }
    }
  } else {
    // ＋meta 定义 isauther  循环meta中isAuther为false 都跳login 其他放行???

    //未登录：不能去交易相关、不能去支付相关【pay|paysuccess】、不能去个人中心
    //未登录去上面这些路由-----登录

    let toPath = to.path

    // let nextRoute = ['/trade', '/pay', '/center','/shopcart']
    //当前进入的页面，是不是需要验证哪些页面
    /* if (nextRoute.indexOf(toPath) >= 0) {
     //把未登录的时候向去而没有去成的信息，存储于地址栏中【路由】
     next('/login?redirect=' + toPath)
    } else {
      next()
    } */

    if (
      // 判断topath是否包含那些字符串,这里'/pay'同时也能筛选出 /pay /paysuccess
      toPath.indexOf('/trade') != -1 ||
      toPath.indexOf('/pay') != -1 ||
      toPath.indexOf('/center') != -1
    ) {
      //把未登录的时候向去而没有去成的信息，存储于地址栏中【路由】,去登入按钮判断
      next('/login?redirect=' + toPath)
    } else {
      //去的不是上面这些路由（home|search|shopCart）---放行
      next()
    }
  }
})

export default router
