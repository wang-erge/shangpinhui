import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/css/reset.css'
import VueLazyload from 'vue-lazyload'

//引入自定义插件
/* import myPlugins from '@/plugins/myPlugins.js';
Vue.use(myPlugins,{
    name:'upper'
});
 */

//引入表单校验插件
import '@/plugins/validate.js'

// 传入一个配置对象，设置懒加载默认的 loading图片
Vue.use(VueLazyload, {
  loading: require('@/assets/images/loding.gif')
})

//将三级联动组件注册为全局组件
import TypeNav from '@/components/TypeNav.vue'
import Carousel from '@/components/Carousel.vue'
import Pagination from '@/components/Pagination.vue'

//第一个参数：全局组件名字，第二个参数：全局组件
Vue.component(TypeNav.name, TypeNav)
Vue.component(Carousel.name, Carousel)
Vue.component(Pagination.name, Pagination)
import { Message, MessageBox } from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.prototype.$message = Message
Vue.prototype.$alert = MessageBox.alert

import '@/mock/mockServe.js'
import 'swiper/css/swiper.css'

Vue.config.productionTip = false

// 暴露所有接口，并且挂载到vue原型上
import * as API from '@/api/index.js'

new Vue({
  router,
  store,
  render: (h) => h(App),
  // 全局事件总线 $bus
  beforeCreate() {
    Vue.prototype.$bus = this
    Vue.prototype.$API = API
  }
}).$mount('#app')
