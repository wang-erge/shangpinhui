import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import home from './modules/home.js'
import search from './modules/search.js'
import detail from './modules/detail.js'
import shopcart from './modules/shopcart.js'
import user from './modules/user.js'
import trade from './modules/trade.js'

export default new Vuex.Store({
  modules: {
    home,
    search,
    detail,
    shopcart,
    user,
    trade
  }
})
