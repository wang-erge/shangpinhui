import {
  reqgetCategoryList,
  reqGetBannerList,
  reqFloorList
} from '@/api/index.js'

export default {
  // namespaced: true, //开启命名空间
  state: {
    categoryList: [], //home三级菜单
    bannerList: [], //轮播图
    floorList: [] //floor组件
  },
  //mutions是唯一修改state的地方
  mutations: {
    GETCATEGORYLIST(state, categoryList) {
      state.categoryList = categoryList
    },
    GETBANNERLIST(state, bannerList) {
      state.bannerList = bannerList
    },
    GETFLOORLIST(state, floorList) {
      state.floorList = floorList
    }
  },
  //action |用户处理派发action地方的，可以书写异步语句、自己逻辑地方
  actions: {
    async getCategoryList({ commit }) {
      //reqgetCategoryList返回的是一个Promise对象
      let result = await reqgetCategoryList()
      if (result.code == 200) {
        commit('GETCATEGORYLIST', result.data)
      }
    },
    //获取首页轮播图的数据
    async getBannerList({ commit }) {
      let result = await reqGetBannerList()
      console.info(result)
      if (result.code == 200) {
        commit('GETBANNERLIST', result.data)
      }
    },
    //获取floor数据
    async getFloorList({ commit }) {
      let result = await reqFloorList()
      if (result.code == 200) {
        //提交mutation
        commit('GETFLOORLIST', result.data)
      }
    }
  },
  getters: {}
}
